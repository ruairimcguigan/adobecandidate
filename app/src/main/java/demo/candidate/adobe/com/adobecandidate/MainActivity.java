package demo.candidate.adobe.com.adobecandidate;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import demo.candidate.adobe.com.adobecandidate.model.Candidate;
import demo.candidate.adobe.com.adobecandidate.utils.CustomSpinnerAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    EditText mFirstName, mSurname, mEmailAddress;
    Spinner mInterestDropdown;
    Button mSubmitBtn;
    String dropDownSelect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initViews();
        initCustomSpinner();
        clickListeners();
    }

    private void clickListeners() {
        mSubmitBtn.setOnClickListener(this);
    }

    private void initCustomSpinner() {
        CustomSpinnerAdapter spinnerAdapter = new CustomSpinnerAdapter
                (this, android.R.layout.simple_spinner_dropdown_item,
                        getResources().getStringArray(R.array.interest_options));
        mInterestDropdown.setAdapter(spinnerAdapter);
    }

    /**
     * UI component views initiated and inflated
     */
    private void initViews() {
        mFirstName = (EditText)findViewById(R.id.candidate_firstname_input);
        mSurname = (EditText) findViewById(R.id.candidate_surname_input);
        mEmailAddress = (EditText) findViewById(R.id.candidate_email_input);
        mInterestDropdown = (Spinner) findViewById(R.id.interest_spinner);
        mSubmitBtn = (Button) findViewById(R.id.submit_button);
    }

    /**
     * Toolbar items - inflated
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Toolbar items - click actions and event handlers
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
         Candidate mCandidate = new Candidate();
        if(v.getId() == R.id.submit_button){



            /**
             * We've built a basic data object that will serve to stored candidate details.
             * Data fields are set on submission of form using the click event on submit button
             */
            mCandidate.setFirstName(mFirstName.getText().toString());
            mCandidate.setSurname(mSurname.getText().toString());
            mCandidate.setEmail(mEmailAddress.getText().toString());

            /**
             * Capture and send user input
             */
            submitSpinner(mCandidate);

            Toast.makeText(MainActivity.this, "Candidate saved", Toast.LENGTH_SHORT).show();

        }
    }

    private void submitSpinner(final Candidate mCandidate) {
        mInterestDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dropDownSelect = mInterestDropdown.getItemAtPosition(position).toString();
                mCandidate.setInterest(dropDownSelect);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Toast.makeText(MainActivity.this, getApplicationContext().getResources()
                        .getString(R.string.nothing_selected), Toast.LENGTH_SHORT).show();
            }
        });
    }
}
