package demo.candidate.adobe.com.adobecandidate.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import demo.candidate.adobe.com.adobecandidate.R;


public class CustomFontTextView extends TextView {
    private Typeface mType;

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public String text() {
        return this.getText().toString();
    }

    public void init() {
        mType = getCustomTypeface(getContext());
        setTypeface(mType);
    }

    /**
     * Make Typeface available for use in other components
     *
     * @param context
     * @return the typeface for the custom font
     */
    public static Typeface getCustomTypeface(Context context) {
        return Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.custom_font_path));
    }
}

